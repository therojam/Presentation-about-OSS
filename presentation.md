<!-- $theme: default -->



# Welche Info-Möglichkeiten gibts zu OSS?
## Und wie finde alternativen zu meiner Software? 

---

## Gründe für die Nutzung von OSS :memo:

1. Unabhängigkeit von einem Hersteller :unlock:
2. Maximale Flexibilität ☮️
3. Hardwareplattform-Unabhängigkeit :computer:
5. Sicherheit :cop:
6. Kosteneinsparungen :dollar:
7. Wirtschaftsförderung :briefcase:



> OSS in öffentlichen Einrichtungen   
> :information_source: [Wikipedia](https://de.wikipedia.org/wiki/Open-Source-Software_in_öffentlichen_Einrichtungen)



--- 

## Recherche :mag:

---

#### :one: viele Artikel oder Hinweise zu aktuellen Umstiegsplänen von öffentlicher Verwaltung
- Schleswig-Holstein (Unabhängigkeit) 
	-  https://www.heise.de/newsticker/meldung/Open-Source-vor-Schleswig-Holstein-will-sich-vollstaendig-von-Microsoft-loesen-4079834.html
	 - https://www.my-it-brain.de/wordpress/__trashed/
- Dortmund 
 	- http://www.crn.de/software-services/artikel-116824.html
- Barcelona (Public Money - Public Code)
	- https://www.golem.de/news/verwaltung-barcelona-plant-wechsel-auf-open-source-software-1801-132177.html
- Münster
	- https://www.stadt-muenster.de/sessionnet/sessionnetbi/vo0050.php?__kvonr=2004043370

---

#### :two: alte Negativ Artikel über den Umstieg auf OpenSource mit Verweis auf Studien (M$-Studien)
- hier geht es in der Regel um den Kostennutzenfaktor
- sowas macht es nicht einfach
- https://www.pcwelt.de/ratgeber/Ratgeber-Software-3424149.html
- https://www.channelpartner.de/a/gartner-umstieg-auf-open-source-lohnt-nicht,209809
---

####  :three: Artikel / Anleitungen für den Umstieg
- [heise.de](https://www.heise.de/newsticker/meldung/Linux-Ein-und-Umstieg-leicht-gemacht-4062580.html?hg=1&hgi=1&hgf=false)
-  https://www.umstieg-auf-linux.de/
-  [FastWP - Mein Umstieg auf Ubuntu Linux](https://fastwp.de/umstieg-ubuntu-linux/) mit Alternativen zu gängiger Programme
-  [Chip.de - Umstieg auf Linux Mint](https://www.chip.de/bildergalerie/Umstieg-auf-Linux-Mint-So-geht-s-Galerie_39563673.html)
-  [Warum Linux besser ist](http://www.warumlinuxbesserist.de/umstieg-von-windows-zu-linux/)

---

## Wie finde Alternativen zu bekannten Programmen?

> :link: alternativeto.net

---

## alternativeTo

- Suchfunktion 
  - mit Vorschlägen
- Filter
  - Lizenz auswählbar
  - Plattform (Betriebsystem) auswählbar

---

![Screenshot OS-Alternativen zu MS Word](/Users/amueller/Desktop/AltoNetWord.png)

---

> **Author:** Anton Mueller
> Diese Präsentation frei verfügbar unter
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.de)
> :globe_with_meridians: https://gitlab.com/therojam/Presentation-about-OSS/