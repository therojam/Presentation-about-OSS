# Präsentation zu Info-Möglichkeiten über Open Source Software
Diese Präsentation ist im Rahmen der [#WocheDerDigitalisierung](https://twitter.com/hashtag/WocheDerDigitalisierung?src=hash) von [ver.di](https://verdi.de) entstanden.
Damit jeder den Inhalt versteht ist sie sehr einfach gehalten und nicht vollständig.
## Tools :wrench:
Erstellt wurde es mit  der OpenSource-Software [marp](https://yhatt.github.io/marp/).  
Marp ist ein spezieller Editor für die einfache Erstellung von Präsentationen in der Auszeichnungssprache [MarkDown](https://guides.github.com/features/mastering-markdown/).
## Lizenz :warning:
Diese Präsentation frei verfügbar unter [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.de)!